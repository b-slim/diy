//---------------------------------------------------------------------------
//
// Example of using DIY to perform asynchronous sending
//  and receiving of data to remote blocks
//
// Tom Peterka
// Argonne National Laboratory
// 9700 S. Cass Ave.
// Argonne, IL 60439
// tpeterka@mcs.anl.gov
//
// (C) 2011 by Argonne National Laboratory.
// See COPYRIGHT in top-level directory.
//
//--------------------------------------------------------------------------
#include <mpi.h>
#include <stdlib.h>
#include <stddef.h>
#include "diy.h"

using namespace std;

//--------------------------------------------------------------------------
//
// main
//
int main(int argc, char **argv) {

  int dim = 3; // number of dimensions in the problem
  int tot_blocks = 8; // total number of blocks
  int data_size[3] = {10, 10, 10}; // data size 10x10x10
  int min[3], size[3]; // block extents
  int given[3] = {0, 0, 0}; // constraints on blocking (none so far)
  int ghost[6] = {0, 0, 0, 0, 0, 0}; // -x, +x, -y, +y, -z, +z ghost
  char *infiles[] = { (char *)"test.dat" };
  int num_threads = 1; // number of threads DIY can use
  int nblocks; // my local number of blocks
  int rank; // MPI process
  int did; // domain id

  // initialize MPI
  if (num_threads > 1) {
    int thread_level; // threading level that MPI implementation provides
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &thread_level);
    assert(thread_level == MPI_THREAD_FUNNELED);
  } else
    MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  // examples don't do any error checking, but real apps should

  // initialize DIY after initializing MPI
  DIY_Init(dim, num_threads, MPI_COMM_WORLD);

  // decompose domain
  did = DIY_Decompose(ROUND_ROBIN_ORDER, data_size, tot_blocks, &nblocks, 
		      1, ghost, given, 0);

//   // read data, assume integer, raw format
//   int *data[nblocks];
//   memset(data, 0, sizeof(int*) * nblocks); // memset tells DIY to allocate data
//   DIY_Read_data_all(did, infiles, DIY_INT, (void**)data, 0);

//   // test DIY point to point asynchronous communication by sending
//   // the first 4 data values (for example) in a ring of blocks 
//   // ordered by gid
//   for (int i = 0; i < nblocks; i++) {
//     int dest_gid = (DIY_Gid(did, i) + 1 ) % tot_blocks;
//     DIY_Send(did, i, data[i], 4, MPI_INT, dest_gid);
//   }

//   void **recv_data = new void*[1];
//   int src_gids[1]; // only one source for each local block in this example
//   int sizes[1]; // size of each item in datatype units (not bytes)
//   int count; // number of received items (should be 1 in this example)
//   for (int i = 0; i < nblocks; i++) {
//     DIY_Recv(did, i, recv_data, &count, 1, MPI_INT, src_gids, sizes);
//     assert(count == 1);
//     // MPI-3 one-sided implementation of DIY_Send and DIY_Recv 
//     // provides valid src_gids, but MPI-2 two-sided implementation does not
//     if (src_gids[0] >= 0) // print src gids if available
//       fprintf(stderr, "gid %d received data values %d - %d from gid %d\n",
// 	      DIY_Gid(did, i), DIY_Recvd_item(int, recv_data, 0)[0], 
// 	      DIY_Recvd_item(int, recv_data, 0)[3], src_gids[0]);
//     else // otherwise don't print them
//       fprintf(stderr, "gid %d received data values %d - %d\n",
// 	      DIY_Gid(did, i), DIY_Recvd_item(int, recv_data, 0)[0], 
// 	      DIY_Recvd_item(int, recv_data, 0)[3]);
//   }

//   // test DIY remote block table lookup by asking which process owns
//   // each global block
//   // requires giving DIY > 1 thread
//   if (num_threads > 1) {
//     for (int gid = 0; gid < DIY_Tot_num_gids(); gid++) {
//       int owner_proc = DIY_Proc(gid);
//       fprintf(stderr, "The owner of gid %d is process %d\n", 
// 	      gid, owner_proc);
//     }
//   }

//   DIY_Flush_send_recv(0);

#if 1
  // test DIY remote block table lookup by asking which process owns
  // each global block
  // requires giving DIY > 1 thread
  for (int gid = 0; gid < DIY_Tot_num_gids(); gid++) {
    int owner_proc = DIY_Proc(gid);
    fprintf(stderr, "The owner of gid %d is process %d\n", 
	    gid, owner_proc);
  }
#endif

  // cleanup
  DIY_Finalize();
  MPI_Finalize();

  fflush(stderr);
  if (rank == 0)
    fprintf(stderr, "\n---Completed successfully---\n");
  return 0;

}
